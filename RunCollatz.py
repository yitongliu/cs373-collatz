#!/usr/bin/env python3

# ---------------------
# collatz/RunCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ---------------------

# -------
# imports
# -------

"""
run the program
"""

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
