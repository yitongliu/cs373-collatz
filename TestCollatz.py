
#!/usr/bin/env python3

# ----------------------
# collatz/TestCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ----------------------

# -------
# imports
# -------
"""
test function
"""
from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    """
    test functions in Collatz.py
    """
    # ----
    # read
    # ----

    def test_read(self):
        """test collatz_read"""
        reader = "1 10\n"
        i, j = collatz_read(reader)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """unit test 1"""
        res = collatz_eval(1, 10)
        self.assertEqual(res, 20)

    def test_eval_2(self):
        """unit test 2"""
        res = collatz_eval(100, 200)
        self.assertEqual(res, 125)

    def test_eval_3(self):
        """unit test 3"""
        res = collatz_eval(201, 210)
        self.assertEqual(res, 89)

    def test_eval_4(self):
        """unit test 4"""
        res = collatz_eval(900, 1000)
        self.assertEqual(res, 174)

    def test_eval_5(self):
        """unit test 5"""
        res = collatz_eval(1, 1)
        self.assertEqual(res, 1)

    def test_eval_6(self):
        """unit test 6"""
        res = collatz_eval(53, 55)
        self.assertEqual(res, 113)

    def test_eval_7(self):
        """unit test 7"""
        res = collatz_eval(10000, 20000)
        self.assertEqual(res, 279)

    def test_eval_8(self):
        """unit test 8"""
        res = collatz_eval(32, 32)
        self.assertEqual(res, 6)

    def test_eval_9(self):
        """unit test 9"""
        res = collatz_eval(1, 1000000)
        self.assertEqual(res, 525)

    def test_eval_10(self):
        """unit test 10"""
        res = collatz_eval(1000000, 1)
        self.assertEqual(res, 525)

    # -----
    # print
    # -----

    def test_print(self):
        """test collatz_print"""
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """test collatz_solve"""
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
