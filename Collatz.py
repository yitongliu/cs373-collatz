"""
#!/usr/bin/env python3

# ------------------
# collatz/Collatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ------------------"""

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(line: str) -> List[int]:
    """
    read two ints
    s a string
    return a LST of two ints, representing the beginning and end of a range, [i, j]
    """
    num = line.split()
    return [int(num[0]), int(num[1])]


# ------------
# collatz_eval
# ------------
DIC = {1: 1}
LST = [1]


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # if cache haven't been built
    if 2 not in DIC:
        initialize()
    # consider situation when i > j, prevent error
    if i > j:
        i = i + j
        j = i - j
        i = i - j
    # check the largest element of LST in the interval [i,j], save time
    index = 0
    while index < len(LST) and LST[index] <= j:
        index += 1
    if LST[index - 1] >= i:
        return cycle_length(LST[index - 1])
    ans = 0
    for num in range(i, j + 1):
        if DIC[num] > ans:
            ans = DIC[num]
    return ans


def cycle_length(num: int) -> int:
    """
    calculate the cycle_length of number num
    """
    cnt = 1
    while True:
        # keep taking "dividing 2" branch until num is odd
        while (num % 2) == 0:
            num = num // 2
            cnt += 1
            if num == 1:
                return cnt
        # check cache
        if num in DIC:
            return cnt + DIC[num] - 1
        # taking "times 3 plus 1" branch once, and "dividing 2" branch once
        num += (num >> 1) + 1
        cnt += 2
# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], i: int, j: int, mcl: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    writer.write(str(i) + " " + str(j) + " " + str(mcl) + "\n")


def initialize():
    """
    initialize
    DIC is the eager cache, store the cycle length of all number between 1 and 1000000
    LST is a list of numbers with a cycle length longer than that of any smaller value
    call the numbers in LST climaxes, if there are one or more climaxes between i and j,
    then the largest climax have the maximum cycle length between i and j
    """
    ans = 0
    for num in range(2, 1000001):
        cnt = cycle_length(num)
        if cnt > ans:
            LST.append(num)
            ans = cnt
        DIC[num] = cnt
# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for line in reader:
        i, j = collatz_read(line)
        ans = collatz_eval(i, j)
        collatz_print(writer, i, j, ans)
